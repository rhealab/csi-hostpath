# Hostpath CSI

## Overview

Hostpath CSI plugin implements an CSI interface. It create xfs quota local disk map to pod's container.

## Requirements
* Separate local disks(/dev/sdb1 /dev/sdc1).
* Format and mount.
```
mkfs.xfs /dev/sdb1
mkfs.xfs /dev/sdc1
mount -o prjquota /dev/sdb1 /xfs/disk1
mount -o prjquota /dev/sdc1 /xfs/disk2
```
* Label has quota disk node.
```
kubectl label node $nodename io.enndata/hasquotapath=true
```

## Deploy hostpath CSI
```
kubectl create -f plugin.yaml -f attacher.yaml
```

## Create test pod
```
kubectl create -f keeptruepv.yaml -f keeptruepvc.yaml -f app-rc.yaml
```


